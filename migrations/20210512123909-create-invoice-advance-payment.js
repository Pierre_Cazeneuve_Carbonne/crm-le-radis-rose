'use strict';
module.exports = {
    up: async (queryInterface, DataTypes) => {
        await queryInterface.createTable('Invoice_Advance_Payments', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER,
            },
            estimate_name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            estimate_user_id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                references: {
                    model: "Users",
                    key: "id"
                }
            },
            estimate_customer_document_id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                references: {
                    model: "Customer_Documents",
                    key: "id"
                }
            },
            estimate_document_send: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            estimate_notes: {
                type: DataTypes.STRING,
                allowNull: false
            },
            isarchived: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            invoice_advance_payment_status: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            invoice_advance_payment_estimate_id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                references: {
                    model: "Estimates",
                    key: "id"
                }
            },
            invoice_advance_payment_percent: {
                type: DataTypes.INTEGER,
                allowNull: false

            },
            invoice_advance_payment_total: {
                type: DataTypes.DECIMAL,
                allowNull: false
            },
            createdAt: {
                allowNull: false,
                type: DataTypes.DATE
            },
            updatedAt: {
                allowNull: false,
                type: DataTypes.DATE
            }
        });
    },
    down: async (queryInterface, DataTypes) => {
        await queryInterface.dropTable('Invoice_Advance_Payments');
    }
};