'use strict';
module.exports = {
    up: async (queryInterface, DataTypes) => {
        await queryInterface.createTable('Estimates', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER
            },
            estimate_name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            estimate_user_id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                references: {
                    model: "Users",
                    key: "id"
                }
            },
            estimate_customer_document_id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                references: {
                    model: "Customer_Documents",
                    key: "id"
                }
            },
            estimate_document_send: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            estimate_notes: {
                type: DataTypes.STRING,
                allowNull: false
            },
            isarchived: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            estimate_validation_date: {
                type: DataTypes.DATE,
                allowNull: false
            },
            estimate_relaunch_date: {
                type: DataTypes.DATE,
                allowNull: false
            },
            estimate_status: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            estimate_total: {
                type: DataTypes.DECIMAL,
                allowNull: false
            },
            estimate_invoice_id: {
                type: DataTypes.INTEGER,
                allowNull: true,
                references: {
                    model: "Invoices",
                    key: "id"
                }
            },
            createdAt: {
                allowNull: false,
                type: DataTypes.DATE
            },
            updatedAt: {
                allowNull: false,
                type: DataTypes.DATE
            }
        });
    },
    down: async (queryInterface, DataTypes) => {
        await queryInterface.dropTable('Estimates');
    }
};