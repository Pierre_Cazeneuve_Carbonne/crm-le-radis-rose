'use strict';
module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.createTable('Products', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      product_label: {
        type: DataTypes.STRING,
        allowNull:false
      },
      product_type: {
        type: DataTypes.STRING,
        allowNull:false
      },
      product_tax: {
        type: DataTypes.DECIMAL,
        allowNull:false
      },
      product_decription: {
        type: DataTypes.STRING,
        allowNull:true
      },
      product_unit_price: {
        type: DataTypes.DECIMAL,
        allowNull:false
      },
      product_buying_price: {
        type: DataTypes.DECIMAL,
        allowNull:true
      },
      product_service_days: {
        type: DataTypes.INTEGER,
        allowNull:true
      },
      product_service_day_price: {
        type: DataTypes.DECIMAL,
        allowNull:true
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
      }
    });
  },
  down: async (queryInterface, DataTypes) => {
    await queryInterface.dropTable('Products');
  }
};