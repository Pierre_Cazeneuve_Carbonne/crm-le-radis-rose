'use strict';
module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.createTable('Customers', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      customer_phone: {
        type: DataTypes.STRING,
        allowNull: false
      },
      customer_website: {
        type: DataTypes.STRING,
        allowNull: true
      },
      customer_name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      customer_siret: {
        type: DataTypes.STRING,
        allowNull: false
      },
      customer_code_postal: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      customer_city: {
        type: DataTypes.STRING,
        allowNull: false
      },
      customer_surname: {
        type: DataTypes.STRING,
        allowNull: false
      },
      customer_entreprise_name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      customer_entreprise_logo: {
        type: DataTypes.BLOB,
        allowNull: true
      },
      customer_notes: {
        type: DataTypes.STRING,
        allowNull: true
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
      }
    });
  },
  down: async (queryInterface, DataTypes) => {
    await queryInterface.dropTable('Customers');
  }
};