'use strict';
module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.createTable('Parameter_Settings', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      accounting_setting_name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      accounting_setting_type: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "Parameter_Types",
          key: "id"
        }
      },
      accounting_setting_value: {
        type: DataTypes.STRING,
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
      }
    });
  },
  down: async (queryInterface, DataTypes) => {
    await queryInterface.dropTable('Parameter_Settings');
  }
};