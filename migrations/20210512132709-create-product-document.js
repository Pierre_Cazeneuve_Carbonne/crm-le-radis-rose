'use strict';
module.exports = {
    up: async (queryInterface, DataTypes) => {
        await queryInterface.createTable('Product_Documents', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: DataTypes.INTEGER
            },
            product_label: {
                type: DataTypes.STRING,
                allowNull: false
            },
            product_type: {
                type: DataTypes.STRING,
                allowNull: false
            },
            product_tax: {
                type: DataTypes.DECIMAL,
                allowNull: false
            },
            product_decription: {
                type: DataTypes.STRING,
                allowNull: false
            },
            product_unit_price: {
                type: DataTypes.DECIMAL,
                allowNull: false
            },
            product_buying_price: {
                type: DataTypes.DECIMAL,
                allowNull: true
            },
            product_service_days: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            product_service_day_price: {
                type: DataTypes.DECIMAL,
                allowNull: true
            },
            product_document_quantity: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            product_document_invoice_id: {
                type: DataTypes.INTEGER,
                allowNull: true,
                references: {
                    model: "Invoices",
                    key: "id"
                }
            },
            product_document_estimate_id: {
                type: DataTypes.INTEGER,
                allowNull: true,
                references: {
                    model: "Estimates",
                    key: "id"
                }
            },
            product_document_invoice_advance_payment: {
                type: DataTypes.INTEGER,
                allowNull: true,
                references: {
                    model: "Invoice_Advance_Payments",
                    key: "id"
                }
            },
            createdAt: {
                allowNull: false,
                type: DataTypes.DATE
            },
            updatedAt: {
                allowNull: false,
                type: DataTypes.DATE
            }
        });
    },
    down: async (queryInterface, DataTypes) => {
        await queryInterface.dropTable('Product_Documents');
    }
};