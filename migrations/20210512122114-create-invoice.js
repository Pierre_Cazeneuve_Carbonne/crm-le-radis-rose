'use strict';
module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.createTable('Invoices', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      invoice_date: {
        type: DataTypes.DATE,
        allowNull: false
      },
      invoice_status: {
        type: DataTypes.BOOLEAN,
        allowNull: false
      },
      invoice_total: {
        type: DataTypes.DECIMAL,
        allowNull: false
      },
      invoice_delay_date: {
        type: DataTypes.DATE,
        allowNull: false
      },
      invoice_name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      invoice_user_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "Users",
          key: "id"
    }
      },
      invoice_customer_document_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "Customer_Documents",
          key: "id"
        }
      },
      invoice_document_send: {
        type: DataTypes.BOOLEAN,
        allowNull: false
      },
      invoice_document_notes: {
        type: DataTypes.STRING,
        allowNull: false
      },
      isarchived: {
        type: DataTypes.BOOLEAN,
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
      }
    });
  },
  down: async (queryInterface, DataTypes) => {
    await queryInterface.dropTable('Invoices');
  }
};