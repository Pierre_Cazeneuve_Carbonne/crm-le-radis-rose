'use strict';
module.exports = {
    up: async (queryInterface, DataTypes) => {
        await queryInterface.createTable('Users', {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true,
                allowNull: false,
                unique: true
            },
            user_name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            user_surname: {
                type: DataTypes.STRING,
                allowNull: false
            },
            user_phone: {
                type: DataTypes.STRING,
                allowNull: false
            },
            user_email: {
                type: DataTypes.STRING,
                unique: true,
                allowNull: false,
                validate: {
                    isEmail: true
                }
            },
            user_password: {
                type: DataTypes.STRING,
                allowNull: false
            },
            user_function: {
                type: DataTypes.STRING,
                allowNull: true
            },
            user_data_hiring: {
                type: DataTypes.DATE,
                allowNull: true
            },
            user_data_departure: {
                type: DataTypes.STRING,
                allowNull: true
            },
            user_notes: {
                type: DataTypes.STRING,
                allowNull: true
            },
            user_role: {
                type: DataTypes.INTEGER,
                allowNull: false,
                references: {
                    model: "Roles",
                    key: "id"
                }
            },
            createdAt: {
                type: DataTypes.DATE,
                allowNull: false
            },
            updatedAt: {
                type: DataTypes.DATE,
                allowNull: false
            }
        });
    },
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable('Users');
    }
};