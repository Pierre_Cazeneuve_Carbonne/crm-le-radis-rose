'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Invoice extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.Product_Document)
    }
  };
  Invoice.init({
    invoice_date: {
      type: DataTypes.DATE,
      allowNull: false
    },
    invoice_status: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    invoice_total: {
      type: DataTypes.DECIMAL,
      allowNull:false
    },
    invoice_delay_date: {
      type: DataTypes.DATE,
      allowNull: false
    },
    invoice_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    invoice_user_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    invoice_customer_document_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    invoice_document_send: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    invoice_document_notes: {
      type: DataTypes.STRING,
      allowNull: false
    },
    isarchived: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'Invoice',
  });
  return Invoice;
};