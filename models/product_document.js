'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Product_Document extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Product_Document.init({
    product_label: {
      type: DataTypes.STRING,
      allowNull: false
    },
    product_type: {
      type: DataTypes.STRING,
      allowNull: false
    },
    product_tax: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    product_decription: {
      type: DataTypes.STRING,
      allowNull: false
    },
    product_unit_price: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    product_buying_price: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    product_service_days: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    product_service_day_price: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    product_document_quantity: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    product_document_invoice_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    product_document_estimate_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    product_document_invoice_advance_payment: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  }, {
    sequelize,
    modelName: 'Product_Document',
  });
  return Product_Document;
};