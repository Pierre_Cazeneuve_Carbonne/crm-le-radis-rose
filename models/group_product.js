'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Group_Product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Group_Product.init({
    group_product_product_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    group_product_group_id: {
      type: DataTypes.INTEGER,

    }
  }, {
    sequelize,
    modelName: 'Group_Product',
  });
  return Group_Product;
};