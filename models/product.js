'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsToMany(models.Group, { through: "Group_Products"})
    }
  };
  Product.init({
    product_label: {
      type: DataTypes.STRING,
      allowNull:false
    },
    product_type: {
      type: DataTypes.STRING,
      allowNull:false
    },
    product_tax: {
      type: DataTypes.DECIMAL,
      allowNull:false
    },
    product_decription: {
      type: DataTypes.STRING,
      allowNull:true
    },
    product_unit_price: {
      type: DataTypes.DECIMAL,
      allowNull:false
    },
    product_buying_price: {
      type: DataTypes.DECIMAL,
      allowNull:true
    },
    product_service_days: {
      type: DataTypes.INTEGER,
      allowNull:true
    },
    product_service_day_price: {
      type: DataTypes.DECIMAL,
      allowNull:true
    }
  }, {
    sequelize,
    modelName: 'Product',
  });
  return Product;
};