'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  User.init({
    user_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    user_surname: {
      type: DataTypes.STRING,
      allowNull: false
    },
    user_phone: {
      type: DataTypes.STRING,
      allowNull: false
    },
    user_email: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
      validate: {
        isEmail: true
      }
    },
    user_password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    user_function: {
      type: DataTypes.STRING,
      allowNull: true
    },
    user_data_hiring: {
      type: DataTypes.DATE,
      allowNull: true
    },
    user_data_departure: {
      type: DataTypes.STRING,
      allowNull: true
    },
    user_notes: {
      type: DataTypes.STRING,
      allowNull: true
    },
    user_role: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};