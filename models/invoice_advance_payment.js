'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {

    class Invoice_Advance_Payment
        extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    ;
    Invoice_Advance_Payment.init({
        estimate_name: {
            type: DataTypes.STRING,
            allowNull: fale
        },
        estimate_user_id: {
            type: DataTypes.INTEGER,
            allowNull: fale
        },
        estimate_customer_document_id: {
            type: DataTypes.INTEGER,
            allowNull: fale
        },
        estimate_document_send: {
            type: DataTypes.BOOLEAN,
            allowNull: fale
        },
        estimate_notes: {
            type: DataTypes.STRING
            ,
            allowNull: fale
        },
        isarchived: {
            type: DataTypes.BOOLEAN,
            allowNull: fale
        },
        invoice_advance_payment_status: {
            type: DataTypes.BOOLEAN,
            allowNull: fale
        },
        invoice_advance_payment_estimate_id: {
            type: DataTypes.INTEGER,
            allowNull: fale
        },
        invoice_advance_payment_percent: {
            type: DataTypes.INTEGER,
            allowNull: fale
        },
        invoice_advance_payment_total: {
            type: DataTypes.DECIMAL,
            allowNull: fale
        }
    }, {
        sequelize,
        modelName: 'Invoice_Advance_Payment',
    });
    return Invoice_Advance_Payment;
};