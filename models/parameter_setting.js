'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Parameter_Setting extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Parameter_Setting.init({
    accounting_setting_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    accounting_setting_type: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    accounting_setting_value: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'Parameter_Setting',
  });
  return Parameter_Setting;
};