'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Estimate extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Estimate.init({
    estimate_name: {
     type: DataTypes.STRING,
      allowNull:false
    },
    estimate_user_id: {
     type: DataTypes.INTEGER,
      allowNull:false
    },
    estimate_customer_document_id: {
     type: DataTypes.INTEGER,
      allowNull:false
    },
    estimate_document_send: {
     type: DataTypes.BOOLEAN,
      allowNull:false
    },
    estimate_notes: {
     type: DataTypes.STRING,
      allowNull:false
    },
    isarchived: {
     type: DataTypes.BOOLEAN,
      allowNull:false
    },
    estimate_validation_date: {
     type: DataTypes.DATE,
      allowNull:false
    },
    estimate_relaunch_date: {
     type: DataTypes.DATE,
      allowNull:false
    },
    estimate_status: {
     type: DataTypes.BOOLEAN,
      allowNull:false
    },
    estimate_total: {
      type: DataTypes.DECIMAL,
      allowNull:false
    },
    estimate_invoice_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    modelName: 'Estimate',
  });
  return Estimate;
};