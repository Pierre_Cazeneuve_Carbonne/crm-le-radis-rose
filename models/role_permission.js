'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Role_Permission extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsToMany(models.User, { through: "Roles_Permissions"})
    }
  };
  Role_Permission.init({
    role_permission_user_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    role_permission_permission_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'Role_Permission',
  });
  return Role_Permission;
};