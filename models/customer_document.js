'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Customer extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models)
    }
  };
  Customer.init({
    customer_phone: {
      type: DataTypes.STRING,
      allowNull: false
    },
    customer_website: {
      type: DataTypes.STRING,
      allowNull: true
    },
    customer_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    customer_siret: {
      type: DataTypes.STRING,
      allowNull: false
    },
    customer_code_postal: {
      type: DataTypes.NUMBER,
      allowNull: false
    },
    customer_city: {
      type: DataTypes.STRING,
      allowNull: false
    },
    customer_surname: {
      type: DataTypes.STRING,
      allowNull: false
    },
    customer_entreprise_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    customer_notes: {
      type: DataTypes.STRING,
      allowNull: true
    },
    customer_entreprise_logo: DataTypes.BLOB
  }, {
    sequelize,
    modelName: 'Customer_Document',
  });
  return Customer;
};