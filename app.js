const express = require('express');
const app = express();
app.use(express.json())
const {sequelize, User, Parameter_Type, Parameter_Setting, Group} = require('./models')


const PORT = 8000;

app.get("/", () => {

})

app.listen(PORT, async () => {
    console.log(`listening on http://localhost:${PORT}`)
    await sequelize.authenticate()
    console.log("database connected !")
})